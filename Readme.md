# Obstacle detector

This repo is meant for **demonstration only** and not for actual flight code.

## Instructions:


Include [`polytraj`](https://gitlab.com/mit-acl/fsw/demos/polytraj) in your workspace

Clone this repo in your workspace and and compile it

Then run: 

```bash
roslaunch obstacle_detector obstacle_detector.launch
rosbag play bags/*.bag
```
