/* ----------------------------------------------------------------------------
 * Copyright 2021, Jesus Tordesillas Torres, Aerospace Controls Laboratory
 * Massachusetts Institute of Technology
 * All Rights Reserved
 * Authors: Jesus Tordesillas, et al.
 * See LICENSE file for the license information
 * -------------------------------------------------------------------------- */
// #include <Eigen/Core>

#include <snapstack_msgs/State.h>
#include <sensor_msgs/PointCloud2.h>
#include "ros/ros.h"
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>

#include <pcl/point_types.h>
#include "pcl_ros/point_cloud.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/geometry.h>

#ifndef OBSTACLE_DETECTOR_HPP
#define OBSTACLE_DETECTOR_HPP

template <typename T>
bool safeGetParam(ros::NodeHandle& nh, std::string const& param_name, T& param_value)
{
  if (!nh.getParam(param_name, param_value))
  {
    ROS_ERROR("Failed to find parameter: %s", nh.resolveName(param_name, true).c_str());
    exit(1);
  }
  return true;
}

void verify(bool cond, std::string info_if_false);

class ObstacleDetector
{
public:
  ObstacleDetector(ros::NodeHandle nh);

  void cloudCB(const sensor_msgs::PointCloud2ConstPtr& input);
  void poseCB(const geometry_msgs::PoseStamped& msg);
  void stateCB(const snapstack_msgs::State& msg);
  void generateTrajectory(Eigen::Vector3d& b_Obs);

protected:
private:
  double x_min_;
  double x_max_;

  double y_min_;
  double y_max_;

  double z_min_;
  double z_max_;

  double leaf_size_filter_;

  ros::Publisher pub_pcloud_filtered_;

  ros::Publisher pub_path_;

  ros::NodeHandle nh_;

  pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud2_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud3_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud4_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud1_;

  ros::Subscriber sub_cloud_;
  ros::Subscriber sub_state_;
  ros::Subscriber sub_pose_;
  std::string name_file_;
  std::string name_drone_;

  // polytraj::TrajectoryGenerator polytraj_;

  geometry_msgs::PoseStamped msg_pose_;      ///< latest vehicle pose
  geometry_msgs::PoseStamped msg_pose_obs_;  ///< latest obstacle vicon pose

  int num_frames_window_ = 5;

  double alpha_filter_ = 1.0;

  double ratio_filtered_ = 0.0;

  double threshold_trigger_stop_ = 0.0;

  Eigen::Vector3d w_current_pos_;
  Eigen::Vector3d w_current_vel_;

  Eigen::Affine3d w_T_b_;
  Eigen::Affine3d b_T_c_;
};

#endif