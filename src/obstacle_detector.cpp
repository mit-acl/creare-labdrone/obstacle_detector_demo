/* ----------------------------------------------------------------------------
 * Copyright 2021, Jesus Tordesillas Torres, Aerospace Controls Laboratory
 * Massachusetts Institute of Technology
 * All Rights Reserved
 * Authors: Jesus Tordesillas, et al.
 * See LICENSE file for the license information
 * -------------------------------------------------------------------------- */

#include <iostream>
#include <ros/ros.h>
#include <pcl/point_types.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl/point_cloud.h>

#include "obstacle_detector.hpp"

#include <pcl_conversions/pcl_conversions.h>

#include <traj_gen/traj_gen.hpp>

#include <chrono>

#include <nav_msgs/Path.h>

#include <pcl/common/common.h>

#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>

using namespace trajgen;
// using namespace Eigen;

ObstacleDetector::ObstacleDetector(ros::NodeHandle nh) : nh_(nh)
{
  name_drone_ = ros::this_node::getNamespace();  // This returns also the slashes (2 in Kinetic, 1 in Melodic)
  name_drone_.erase(std::remove(name_drone_.begin(), name_drone_.end(), '/'), name_drone_.end());  // Remove the slashes

  safeGetParam(nh_, "x_min", x_min_);
  safeGetParam(nh_, "x_max", x_max_);

  safeGetParam(nh_, "y_min", y_min_);
  safeGetParam(nh_, "y_max", y_max_);

  safeGetParam(nh_, "z_min", z_min_);
  safeGetParam(nh_, "z_max", z_max_);

  safeGetParam(nh_, "leaf_size_filter", leaf_size_filter_);
  safeGetParam(nh_, "alpha_filter", alpha_filter_);

  safeGetParam(nh_, "threshold_trigger_stop", threshold_trigger_stop_);

  // Checks
  verify(x_max_ > x_min_, "x_max_>x_min_ must hold");
  verify(y_max_ > y_min_, "y_max_>y_min_ must hold");
  verify(z_max_ > z_min_, "z_max_>z_min_ must hold");

  verify(alpha_filter_ <= 1.0 && alpha_filter_ >= 0.0, "alpha_filter_ in [0,1] must hold");
  verify(threshold_trigger_stop_ <= 1.0 && threshold_trigger_stop_ >= 0.0, "threshold_trigger_stop_ in [0,1] must "
                                                                           "hold");

  pub_pcloud_filtered_ = nh_.advertise<pcl::PointCloud<pcl::PointXYZ>>("/tof/point_cloud_filtered", 1);
  sub_cloud_ = nh_.subscribe("/tof/point_cloud", 1, &ObstacleDetector::cloudCB, this);
  sub_state_ = nh_.subscribe("state", 1, &ObstacleDetector::stateCB, this);

  // sub_pcd_ = nh_.subscribe("/tof/point_cloud", 1, &ObstacleDetector::cloud_cb, this);
  sub_pose_ = nh_.subscribe("/labdrone01/world", 1, &ObstacleDetector::poseCB, this);

  pub_path_ = nh_.advertise<nav_msgs::Path>("path", 1);

  input_cloud2_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
  input_cloud3_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
  input_cloud4_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
  input_cloud_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
  input_cloud1_ = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);

  std::string name_camera = name_drone_ + "/tof";
  while (true)
  {
    tf2_ros::Buffer tf_buffer;
    std::unique_ptr<tf2_ros::TransformListener> tfListener{ new tf2_ros::TransformListener(tf_buffer) };
    geometry_msgs::TransformStamped transform_stamped;
    try
    {
      transform_stamped = tf_buffer.lookupTransform(name_drone_, name_camera, ros::Time(0),
                                                    ros::Duration(0.5));  // Note that ros::Time(0) will just get us the
                                                                          // latest available transform.
      b_T_c_ = tf2::transformToEigen(transform_stamped);                  // Body to camera_depth_optical_frame
      // par_.c_T_b = (par_.b_T_c).inverse();
      break;
    }
    catch (tf2::TransformException& ex)
    {
      ROS_WARN_THROTTLE(1.0, "Trying to find transform %s --> %s", name_drone_.c_str(), name_camera.c_str());
    }
  }
  ROS_INFO("Found transform %s --> %s", name_drone_.c_str(), name_camera.c_str());

  ////

  // hard-coded obstacle location using vicon
  msg_pose_obs_.pose.position.x = 1.00293476489;
  msg_pose_obs_.pose.position.y = -2.14267225608;
  msg_pose_obs_.pose.position.z = 1.6651383667;
}

void ObstacleDetector::stateCB(const snapstack_msgs::State& msg)
{
  // std::cout << "In stateCB!!" << std::endl;
  w_current_pos_ = Eigen::Vector3d(msg.pos.x, msg.pos.y, msg.pos.z);
  w_current_vel_ = Eigen::Vector3d(msg.vel.x, msg.vel.y, msg.vel.z);

  w_T_b_ = Eigen::Translation3d(msg.pos.x, msg.pos.y, msg.pos.z) *
           Eigen::Quaterniond(msg.quat.w, msg.quat.x, msg.quat.y, msg.quat.z);
}

// Everything is done in the camera frame
void ObstacleDetector::cloudCB(const sensor_msgs::PointCloud2ConstPtr& pcl2ptr_msg)
{
  // std::cout << "Converting to PCL " << input_cloud1_->points.size() << " points" << std::endl;

  // Convert to PCL
  pcl::fromROSMsg(*pcl2ptr_msg, *input_cloud1_);

  // Box filter
  pcl::CropBox<pcl::PointXYZ> boxFilter;
  boxFilter.setMin(Eigen::Vector4f(x_min_, y_min_, z_min_, 1.0));
  boxFilter.setMax(Eigen::Vector4f(x_max_, y_max_, z_max_, 1.0));
  boxFilter.setInputCloud(input_cloud1_);
  boxFilter.filter(*input_cloud2_);

  // Voxel grid filter
  pcl::VoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud(input_cloud2_);
  sor.setLeafSize(leaf_size_filter_, leaf_size_filter_, leaf_size_filter_);
  sor.filter(*input_cloud3_);

  // Compute the ratio between the xy area of the box used for cropping and the area with voxels in the xy plane
  double area = (x_max_ - x_min_) * (y_max_ - y_min_);
  double area_filled = input_cloud3_->points.size() * leaf_size_filter_ * leaf_size_filter_;
  double ratio = area_filled / area;

  // Apply lpf
  ratio_filtered_ = (alpha_filter_ * ratio) + (1.0 - alpha_filter_) * ratio_filtered_;

  // Print stuff
  // std::cout << "ratio= " << ratio << std::endl;
  // std::cout << "ratio_filtered= " << ratio_filtered_ << std::endl;
  if (ratio_filtered_ > threshold_trigger_stop_)
  {
    std::cout << "Obstacle detected, triggering stop" << std::endl;

    pcl::PointXYZ minPt, maxPt;
    // https://github.com/PointCloudLibrary/pcl/blob/master/examples/common/example_get_max_min_coordinates.cpp
    pcl::getMinMax3D(*input_cloud3_, minPt, maxPt);

    std::cout << "Min: " << minPt.x << ", " << minPt.y << ", " << minPt.z << std::endl;
    std::cout << "Max: " << maxPt.x << ", " << maxPt.y << ", " << maxPt.z << std::endl;

    // Obstacle = closest point to the camera
    Eigen::Vector3d c_Obs(minPt.x, minPt.y, minPt.z);  // Obstacle expressed in camera frame
    Eigen::Vector3d b_Obs = b_T_c_ * c_Obs;            // Obstacle expressed in body frame

    generateTrajectory(b_Obs);

    pcl::PointXYZ centroid;
    pcl::computeCentroid(*input_cloud3_, centroid);
    // std::cout << "Centroid: " << centroid << std::endl;
    const double dist = std::sqrt(centroid.x * centroid.x + centroid.y * centroid.y + centroid.z * centroid.z);
    std::cout << "Obstacle detected using TOF at distance = " << dist << " m." << std::endl;
    std::cout << "\tVehicle at " << msg_pose_.pose.position.x << ", ";
    std::cout << msg_pose_.pose.position.y << ", ";
    std::cout << msg_pose_.pose.position.z << " in VICON frame." << std::endl;
    std::cout << "\tObstacle at " << msg_pose_obs_.pose.position.x << ", ";
    std::cout << msg_pose_obs_.pose.position.y << ", ";
    std::cout << msg_pose_obs_.pose.position.z << " in VICON frame." << std::endl;
    const double dx = msg_pose_.pose.position.x - msg_pose_obs_.pose.position.x;
    const double dy = msg_pose_.pose.position.y - msg_pose_obs_.pose.position.y;
    const double dz = msg_pose_.pose.position.z - msg_pose_obs_.pose.position.z;
    const double d = std::sqrt(dx * dx + dy * dy + dz * dz);
    std::cout << "\t--> Ground truth distance = " << d << " m." << std::endl;
  }

  // Publish filtered point cloud
  sensor_msgs::PointCloud2 filtered_pcl2_msg;
  pcl::toROSMsg(*input_cloud3_, filtered_pcl2_msg);
  filtered_pcl2_msg.header = pcl2ptr_msg->header;
  pub_pcloud_filtered_.publish(filtered_pcl2_msg);
}

void ObstacleDetector::generateTrajectory(Eigen::Vector3d& b_Obs)
{
  // 1. Parameter setting
  const int dim = 3;
  double t0 = 0.0;
  double tf = 3.0;
  time_knots<double> ts{ t0, 1, 2, tf };
  Eigen::Vector3d objWeights(0, 1, 1);  // Velocity, acceleration, jerk
  uint poly_order = 8, max_conti = 4;
  PolyParam pp(poly_order, max_conti, ALGORITHM::END_DERIVATIVE);  // or ALGORITHM::POLY_COEFF

  // Freeze frames (i.e., not updated by stateCB)
  Eigen::Affine3d b_T_w = w_T_b_.inverse();
  Eigen::Affine3d w_T_b = w_T_b_;

  Eigen::Vector3d b_current_vel = b_T_w.linear() * w_current_vel_;  // vel of b wrt w, expressed in b. NOTE THE
                                                                    // linear(), because vel is a vector, not a point
                                                                    // (i.e, don't want to translate it, only rotate it)
  Eigen::Vector3d b_current_pos = Eigen::Vector3d::Zero();

  std::cout << "w_current_vel_= " << w_current_vel_.transpose() << std::endl;
  std::cout << "b_current_vel= " << b_current_vel.transpose() << std::endl;

  /////// Initial State
  FixPin<double, dim> x0(t0, 0, b_current_pos);
  FixPin<double, dim> xdot0(t0, 1, b_current_vel);
  FixPin<double, dim> xddot0(t0, 2, Eigen::Vector3d::Zero());

  /////// Final State (stop condition)
  // FixPin<double, dim> xf(0.0f, 0, Vector<double, dim>(0, 0, 0));
  FixPin<double, dim> xdotf(tf, 1, Eigen::Vector3d::Zero());
  FixPin<double, dim> xddotf(tf, 2, Eigen::Vector3d::Zero());

  /////// Corridor Constraint for "all t" (discretization)
  std::vector<LoosePin<double, dim>> all_passCube;
  for (double t = t0; t < tf; t = t + 0.05)
  {
    LoosePin<double, dim> passCube(t, 0, -Eigen::Vector3d(2, 2, 2), Eigen::Vector3d(b_Obs.x(), 2, 2));
    all_passCube.push_back(passCube);
  }

  ///////
  std::vector<Pin<double, dim>*> pinSet;  // to prevent downcasting slicing, vector of pointers
  pinSet.push_back(&x0);
  pinSet.push_back(&xdot0);
  pinSet.push_back(&xddot0);
  pinSet.push_back(&xdotf);
  pinSet.push_back(&xddotf);

  for (int i = 0; i < all_passCube.size(); i++)
  {
    pinSet.push_back(&all_passCube[i]);
  }

  /////// Create the object
  PolyTrajGen<double, dim> pTraj(ts, pp);

  bool verbose = false;
  bool isSolved = false;

  pTraj.setDerivativeObj(objWeights);
  pTraj.addPinSet(pinSet);
  isSolved = pTraj.solve(verbose);

  if (!isSolved)
  {
    std::cout << "Failed. " << std::endl;
    return;
  }

  /////// Fill the path msg
  nav_msgs::Path path;
  path.header.frame_id = "world";

  double dc = 0.1;
  d_order d_eval = 0;  // Order of the derivative to evaluate
  for (double t_eval = t0; t_eval <= tf; t_eval = t_eval + dc)
  {
    Eigen::Vector3d b_pos = pTraj.eval(t_eval, 0).transpose();
    Eigen::Vector3d b_vel = pTraj.eval(t_eval, 1).transpose();
    Eigen::Vector3d b_accel = pTraj.eval(t_eval, 2).transpose();

    // Note that we use linear() when applying it to a vector (not a point) (i.e, don't want to translate it, only
    // rotate it)
    Eigen::Vector3d w_pos = w_T_b * b_pos;
    Eigen::Vector3d w_vel = w_T_b.linear() * b_vel;
    Eigen::Vector3d w_accel = w_T_b.linear() * b_accel;

    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.pose.position.x = w_pos.x();
    pose_stamped.pose.position.y = w_pos.y();
    pose_stamped.pose.position.z = w_pos.z();
    path.poses.push_back(pose_stamped);
  }

  pub_path_.publish(path);
}
void ObstacleDetector::poseCB(const geometry_msgs::PoseStamped& msg)
{
  msg_pose_ = msg;
}

void verify(bool cond, std::string info_if_false)
{
  if (cond == false)
  {
    ROS_ERROR("Condition not satisfied:");
    std::cout << info_if_false << std::endl;
    ROS_ERROR("Aborting");
    abort();
  }
}