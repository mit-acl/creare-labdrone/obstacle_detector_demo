/* ----------------------------------------------------------------------------
 * Copyright 2021, Jesus Tordesillas Torres, Aerospace Controls Laboratory
 * Massachusetts Institute of Technology
 * All Rights Reserved
 * Authors: Jesus Tordesillas, et al.
 * See LICENSE file for the license information
 * -------------------------------------------------------------------------- */

#include "obstacle_detector.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "obstacle_detector_node");

  ros::NodeHandle nh("~");

  ObstacleDetector tmp(nh);

  ros::spin();
}
